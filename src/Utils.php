<?php

namespace Drupal\overpass_api;

/**
 * Various utility functions.
 */
class Utils {

  /**
   * Gets lock expire timestamp by id.
   */
  public static function getPersistentLockExpireTimestamp($lockName) {
    return \Drupal::database()->select('semaphore', 's')
      ->fields('s', ['expire'])
      ->condition('s.name', $lockName)
      ->execute()->fetchField();
  }

  /**
   * Gets PHP memory limit.
   */
  public static function getMemoryLimit() {
    return self::returnBytes(ini_get('memory_limit'));
  }

  /**
   * Converts shorthand memory notation value to bytes
   * From http://php.net/manual/en/function.ini-get.php
   *
   * @param $val
   *   Memory size shorthand notation string
   */
  public static function returnBytes($val) {
    $val = trim($val);
    $last = strtolower($val[strlen($val) - 1]);
    $val = substr($val, 0, -1);
    switch ($last) {
      // The 'G' modifier is available since PHP 5.1.0.
      case 'g':
        $val *= 1024;
      case 'm':
        $val *= 1024;
      case 'k':
        $val *= 1024;
    }
    return $val;
  }

}
