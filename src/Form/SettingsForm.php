<?php

namespace Drupal\overpass_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\overpass_api\OverpassApi;
use Drupal\overpass_api\Utils;
use GuzzleHttp\Exception\ClientException;

/**
 * Configure Overpass API settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Name of lock key.
   *
   * @var string
   */
  protected $lockName = 'overpass_api.query_lock';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'overpass_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['overpass_api.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint'),
      '#description' => $this->t('Overpass API endpoint (without <code>interpeter</code> suffix). Common one: <code>http://overpass-api.de/api/</code>'),
      '#default_value' => $this->config('overpass_api.settings')->get('endpoint'),
    ];
    $form['timeout'] = [
      '#type' => 'number',
      '#title' => $this->t('Timeout'),
      '#description' => $this->t('Timeout betweeen several queries in seconds, to prevent rate limit problems.'),
      '#default_value' => $this->config('overpass_api.settings')->get('timeout'),
    ];
    $form['lock_time'] = [
      '#type' => 'number',
      '#title' => $this->t('Lock time'),
      '#description' => $this->t('Minimal time span between each API call in seconds.'),
      '#default_value' => $this->config('overpass_api.settings')->get('lock_time'),
    ];
    $form['repeat_attempts'] = [
      '#type' => 'number',
      '#title' => $this->t('Repeat attempts'),
      '#description' => $this->t('Number of repeats if query is failed with known reasons (timeout, server error, no free slots, etc).'),
      '#default_value' => $this->config('overpass_api.settings')->get('repeat_attempts'),
    ];
    if (\Drupal::service('lock.persistent')->lockMayBeAvailable($this->lockName) == FALSE) {
      $lockExpire = Utils::getPersistentLockExpireTimestamp($this->lockName);
      $form['remove_lock'] = [
        '#type' => 'submit',
        '#prefix' => 'Lock is currently active up to '
        . date('c', $lockExpire) . ' (' . round($lockExpire - time(), 1) . ' sec.)',
          // . \Drupal::service('date.formatter')->format($lockExpire),
        '#value' => $this->t('Remove lock'),
        '#description' => $this->t('Remove lock'),
        '#button_type' => 'secondary',
      ];
    }
    $form['use_slots_timeouts'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use slots timeouts'),
      '#description' => $this->t('Use slots availability time from API /status
      report for timeouts between queries, This is not working properly when API
      is behind proxy with several backends, like in
      http://overpass-api.de/api/status, so by default this option is turned
      off. '),
      '#default_value' => $this->config('overpass_api.settings')->get('use_slots_timeouts'),
    ];
    $form['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable debug mode'),
      '#description' => $this->t('With enabled debug mode all queries are logged to Drupal Logger with `debug` level'),
      '#default_value' => $this->config('overpass_api.settings')->get('debug'),
    ];

    $form['test'] = [
      '#type' => 'details',
      '#title' => $this->t('Test query'),
      '#description' => $this->t('Executes test query to check that all works well.'),
      '#collapsible' => TRUE,
      '#open' => FALSE,
    ];

    $form['test']['query'] = [
      '#type' => 'textarea',
      '#rows' => 12,
      '#default_value' => OverpassApi::getSampleQuery(),
    ];
    $form['test']['test_execute'] = [
      '#type' => 'submit',
      '#value' => $this->t('Execute query'),
      '#button_type' => 'secondary',
      '#weight' => 10,
    ];

    $form['actions']['check_status'] = [
      '#type' => 'submit',
      '#value' => $this->t('Check API status'),
      '#button_type' => 'secondary',
      '#weight' => 10,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!filter_var($form_state->getValue('endpoint'), FILTER_VALIDATE_URL)) {
      $form_state->setErrorByName('endpoint', $this->t('Value must be URL.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $submitButtonId = $form_state->getTriggeringElement()['#parents'][0];

    if ($submitButtonId == 'submit' || $submitButtonId == 'test_execute') {
      $this->config('overpass_api.settings')
        ->set('endpoint', $form_state->getValue('endpoint'))
        ->set('timeout', $form_state->getValue('timeout'))
        ->set('lock_time', $form_state->getValue('lock_time'))
        ->set('repeat_attempts', $form_state->getValue('repeat_attempts'))
        ->set('use_slots_timeouts', $form_state->getValue('use_slots_timeouts'))
        ->set('debug', $form_state->getValue('debug'))
        ->save();
      parent::submitForm($form, $form_state);
    }

    if ($submitButtonId == 'test_execute') {
      $form_state->setRebuild();

      $time = microtime(TRUE);
      try {
        $result = \Drupal::service('overpass_api')->rawQuery($form_state->getValue('query'));
        $duration = microtime(TRUE) - $time;
      }
      catch (\Exception $e) {
        $duration = microtime(TRUE) - $time;
        \Drupal::messenger()
          ->addError('Overpass query failed on '
            . round($duration, 3)
            . ' sec. ' . PHP_EOL . 'Exception: '
            . $e->__toString()
        );
        return;
      }
      \Drupal::messenger()->addMessage(
        new TranslatableMarkup('<div>Overpass query executed successfully, duration: @duration sec. Result:</div>
        <div class="form-textarea-wrapper">
        <textarea rows=12 cols=60 readonly>@result</textarea>
        </div>', [
          '@result' => $result,
          '@duration' => round($duration, 3),
        ])
          );
    }

    if ($submitButtonId == 'remove_lock') {
      \Drupal::service('lock.persistent')->release($this->lockName);
      \Drupal::messenger()->addMessage('Lock successfully removed.');
    }
    if ($submitButtonId == 'check_status') {
      try {
        $time = microtime(TRUE);
        $result = \Drupal::service('overpass_api')->getStatus();
        \Drupal::messenger()->addMessage(
          new TranslatableMarkup('<p>Status reply received in @time seconds:</p><pre>@result</pre>', [
            '@result' => $result,
            '@time' => round(microtime(TRUE) - $time, 2),
          ])
        );
      }
      catch (ClientException $e) {
        \Drupal::messenger()->addError($e->getMessage());

      }
    }
  }

}
