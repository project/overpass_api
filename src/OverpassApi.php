<?php

namespace Drupal\overpass_api;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\overpass_api\DTO\OverpassQueryOptions;
use Drupal\overpass_api\Exception\OverpassApiHttpException;
use Drupal\overpass_api\Exception\OverpassApiMemoryLimitException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;

/**
 * The Overpass API service.
 */
class OverpassApi {

  /**
   * Name of lock key.
   *
   * @var string
   */
  protected $lockName = 'overpass_api.query_lock';

  /**
   * Our HTTP Connector.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The logging channel to use.
   *
   * @var \Psr\Log\LoggerInterface|null
   */
  protected $logger;

  /**
   * User-specified configuration.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Overpass API endpoint.
   *
   * @var string
   */
  protected $endpoint;

  /**
   * Lock time.
   *
   * @var int
   */
  protected $lockTime;

  /**
   * Lock time.
   *
   * @var int
   */
  protected $timeout;

  /**
   * Constructs a new OverpassApi object.
   *
   * @param \GuzzleHttp\Client $client
   *   Client for making HTTP Calls.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Drupal logger.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Drupal configuration.
   */
  public function __construct(Client $client, LoggerChannelFactoryInterface $logger, ConfigFactory $configFactory) {
    $this->logger = $logger->get('overpass_api');
    $this->httpClient = $client;
    $this->config = $configFactory->get('overpass_api.settings');
    $this->endpoint = $this->config->get('endpoint');
    $this->lockTime = $this->config->get('lock_time');
    $this->timeout = $this->config->get('timeout');
    $this->debug = $this->config->get('debug');
    $this->repeatAttempts = $this->config->get('repeat_attempts');
    $this->useSlotsTimeouts = $this->config->get('use_slots_timeouts');
  }

  /**
   * Executes raw overpass query with returning text result.
   *
   * @param string $query
   *   Text of the query.
   *
   * @return string
   *   Raw result string.
   */
  public function rawQuery(string $query) {

    $maxExecutionTime = ($this->timeout + $this->lockTime) * $this->repeatAttempts;
    if (ini_get('max_execution_time') < $maxExecutionTime) {
      // Trying to increase PHP execution time to match max wait time.
      set_time_limit($maxExecutionTime);
      ini_set('max_execution_time', $maxExecutionTime);
    }

    if ($this->useSlotsTimeouts) {
      \Drupal::service('lock.persistent')->wait($this->lockName, $this->timeout);
      \Drupal::service('lock.persistent')->acquire($this->lockName, $this->timeout);
    }

    $this->debug && $this->logger->debug('Executing overpass query: @query', ['@query' => $query]);
    $headers['Accept'] = 'application/json';
    $headers['Content-Type'] = 'application/json';

    $time = microtime(1);

    $queryOptions = [
      'body' => $query,
      'timeout' => $this->timeout,
    ];

    for ($attempt = 1; $attempt <= $this->repeatAttempts; $attempt++) {
      try {
        $response = \Drupal::httpClient()->post($this->getApiCallUrl('interpreter'), $queryOptions);
        if ($response->getStatusCode() == 200) {
          break;
        }
      }
      catch (RequestException | BadResponseException $e) {
        $code = $e->getCode();
        // Workaround with emulating response timeout if we got a timeout from Guzzle
        if($code == 0 && $e instanceof \GuzzleHttp\Exception\ConnectException) {
          $code = 408;
        }
        switch ($code) {
          case 408:
            // 408 Request Timeout.
          case 429:
            // 429 Too Many Requests.
          case 500:
            // 500 Gateway Timeout behind reverse proxy.
          case 504:
            // 504 Gateway Timeout.
            $this->debug && $this->logger->debug('Query failed at attempt @attempt, exception: @exception', [
              '@attempt' => $attempt,
              '@exception' => $e,
            ]);
            sleep($this->lockTime);
            break;

          // 400 Bad Request.
          case 400:
          default:
            // $overpassException = new OverpassApiHttpException($e);
            $this->logger->error('Bad Request: @exception', ['@exception' => $e]);
            throw $e;
        }
        if ($attempt == $this->repeatAttempts) {
          if ($e instanceof ClientException) {
            // Known temporary error, throwing custom exception.
            $overpassException = new OverpassApiHttpException($e);
            $this->logger->error('Temporary errors on request: @exception', ['@exception' => $overpassException]);
            throw $overpassException;
          }
          else {
            $this->logger->error('Query failed on all attempts, last exception: @exception', ['@exception' => $e]);
            throw $e;
          }
        }
      }
    }
    if ($this->useSlotsTimeouts) {
      if ($slotFreeTimeout = $this->getSlotAvailability() !== TRUE) {
        \Drupal::service('lock.persistent')->acquire($this->lockName, $slotFreeTimeout);
      }
    }
    $this->debug && $this->logger->debug('Overpass query executed at attempt @attempt, response time: @responseTime, result size: @resultSize mb, query: @query', [
      '@responseTime' => round(microtime(1) - $time, 2),
      '@resultSize' => round($response->getBody()->getSize() / 1024 / 1024, 1),
      '@query' => $query,
      '@attempt' => $attempt,
    ]);

    $result = $response->getBody()->getContents();

    return $result;
  }

  /**
   * Executes a generated query with options.
   *
   * @param string $query
   *   Text of the base query.
   * @param \Drupal\overpass_api\DTO\OverpassQueryOptions $options
   *   (optional) Object with additional URL options.
   *
   * @return array
   *   Array of overpass objects.
   */
  public function query(string $query, OverpassQueryOptions $options = NULL) {
    if ($options == NULL) {
      $options = new OverpassQueryOptions();
    }
    $timeout = $options->timeout ?? $this->timeout;
    $limitString = $options->limit ?? '';
    $format = $options->outFormat ?? 'json';

    $query = <<<EOT
[out:$format]
[timeout:$timeout];
$query
  {$options->filter};

out {$options->out} {$limitString};
EOT;

    $result = $this->rawQuery($query);

    if ($options->returnRaw) {
      return $result;
    }

    // Overpass often can return result with large size.
    // We need to check data size to prevent memory_limit errors
    // when decoding JSON and further operations.
    $phpMemoryLimit = Utils::getMemoryLimit();
    $phpMemoryUsage = memory_get_usage();
    $dataSize = strlen($result);
    if ($dataSize / ($phpMemoryLimit - $phpMemoryUsage) > 0.6) {
      $exceptionText =
      'Prosessing stopped: Overpass result size ('
      . $dataSize / 1024 / 1024
      . 'mb) is not fit into memory limit ('
      . $phpMemoryLimit / 1024 / 1024
      . 'mb). \nTry to decrease sync chunk size in settings, or increase PHP memory_limit.';
      throw new OverpassApiMemoryLimitException($exceptionText);
    }
    $resultDecoded = json_decode($result);
    // To free up memory.
    unset($result);

    if (!isset($resultDecoded->elements)) {
      $e = new \Exception('Overpass API call returned wrong result');
      $e->result = $result;
      throw $e;
    }
    return $resultDecoded->elements;

  }

  /**
   * Returns a sample Overpass query.
   */
  public static function getSampleQuery(): string {
    $query = <<<'EOT'
[out:json]
[timeout:60];
relation
  ["admin_level"="2"]
  ["type"="boundary"]
  ["boundary"="administrative"]
  ["ISO3166-1:alpha2"~"^..$"];
out tags 3;
EOT;
    return $query;
  }

  /**
   * Retrieves the first slot freeing time from status.
   */
  public function getStatus() {
    $response = \Drupal::httpClient()
      ->get($this->getApiCallUrl('status'));
    $result = $response->getBody()->getContents();
    return $result;
  }

  /**
   * Retrieves the first slot freeing time from status.
   */
  public function getSlotAvailability() {
    $result = $this->getStatus();
    if (preg_match('/\d+ slots available now./', $result)) {
      // We have free slots.
      return TRUE;
    }
    if (preg_match('/Slot available after: (\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z), in (?<seconds>\d+) seconds./', $result, $matches)) {
      return (int) $matches['seconds'];
    }
    return FALSE;
  }

  /**
   * Returns url for api call.
   *
   * @param string $command
   *   Command name.
   *
   * @return string
   *   Url of api call
   */
  public function getApiCallUrl(string $command): string {
    return $this->endpoint
      . (substr($this->endpoint, -1) == '/' ? '' : '/')
      . $command;
  }

  /**
   * Loads OSM element by it's type and id.
   *
   * @param node|relation|way $type
   *   Type of OSM element.
   * @param int $id
   *   Id of OSM element.
   */
  public function getElement(string $type, int $id) {
    $reply = $this->query("$type($id)");
    return $reply[0];
  }

}
