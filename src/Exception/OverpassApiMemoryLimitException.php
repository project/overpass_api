<?php

namespace Drupal\overpass_api\Exception;

/**
 * Defines OverpassApiMemoryLimitException exception class.
 */
class OverpassApiMemoryLimitException extends \Exception {

}
