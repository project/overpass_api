<?php

namespace Drupal\overpass_api\Exception;

use GuzzleHttp\Exception\ClientException;

/**
 * Defines OverpassQueryException exception class.
 */
class OverpassApiHttpException extends \Exception {

  /**
   * Error code.
   *
   * @var code
   */
  protected $code;

  /**
   * Reason phrase.
   *
   * @var reason
   */
  protected $reason;

  /**
   * Full body of Overpass reply.
   *
   * @var body
   */
  protected $body;

  /**
   * Redefine the exception so message isn't optional.
   */
  public function __construct(ClientException $e) {
    $response = $e->getResponse();
    $this->code = $response->getStatusCode();
    $this->reason = $response->getReasonPhrase();
    $this->body = $response->getBody()->getContents();
    $this->message = "{$this->code} {$this->reason}";
    $this->previous = $e;
  }

  /**
   * Custom string representation of object.
   */
  public function __toString() {
    return __CLASS__ . " {$this->code} {$this->reason}\n{$this->body}";
  }

}
