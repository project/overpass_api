<?php

namespace Drupal\overpass_api\DTO;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * Class OverpassQueryOptions.
 *
 * Describes DTO for function Drupal\overpass_api\OverpassApi::query().
 */
class OverpassQueryOptions extends DataTransferObject {
  /**
   * Custom query timeout in seconds.
   *
   * @var int|null
   */
  public ?int $timeout = NULL;

  /**
   * Out format, default is 'qt',
   *
   * @var string
   */

  public ?string $out = 'qt';

  /**
   * Return plaintext result or not, default to FALSE.
   *
   * @var bool
   */
  public ?bool $returnRaw = FALSE;

  /**
   * Limit amount of results, default to NULL.
   *
   * @var int|null
   */
  public ?int $limit = NULL;

  /**
   * Filtering string, default is empty string.
   *
   * @var string
   */
  public ?string $filter = '';

  /**
   * Out format: json, csv, xml.
   *
   * @var string
   */
  public ?string $outFormat = 'json';

}
