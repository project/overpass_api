# Overpass API

Provides API for querying Overpass API service, to use in other Drupal modules.

Module by default uses Overpass API public instance at url
[http://overpass-api.de/api/](http://overpass-api.de/api/), which is slow and
very unstable, so for active usage it is recommended to use other more robust
and stable instance.

But this module tries to handle timeout problems and fatal errors on the
endpoint side with multiple query repeats, to provide more stable work on Drupal
API side.

## Usage example:

```php
$result = \Drupal::service('overpass_api')->query(<<<EOT
relation
  ["admin_level"="2"]
  ["type"="boundary"]
  ["boundary"="administrative"]
  ["ISO3166-1:alpha2"~"^..$"]
EOT);
```
It should return array of OSM elements like this:
```
array:216 [
  0 => {
    "type": "relation"
    "id": 60189
    "members": [...]
    "tags": {...}
  }
  ...
]
```

# Requirements

- Drupal 8 or higher.
- PHP library `spatie/data-transfer-object`, should be installed automatically with Composer.

# Configuration

Configuration form is located on `/admin/config/system/overpass-api` url, where you can configure Overpass API endpoint (default is pointing to public API `http://overpass-api.de/api/`), timeouts and other options.

# Technical details

To prevent timeout errors and fatal problems on default Overpass API endpoint instance, in this module there are implemented correct handling of timeout problems,
fatal errors like `429 Too Many Requests.`, `500 Gateway Timeout behind reverse proxy.` and
`504 Gateway Timeout.` and memory overflow errors to provide more stable work.
